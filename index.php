<?php

require_once ('DatabaseConnect.php');
require_once ('Publication.php');
require_once ('News.php');
require_once ('Article.php');
require_once ('PublicationsViewer.php');

try {
    $newsViewer = new PublicationsViewer('news');
    $articleViewer = new PublicationsViewer('article');
} catch(Exception $error) {
    $error = 'Ошибка базы данных';
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Желтый Вестник: скандалы, интриги, расследования</title>
    <meta charset="utf-8">
    <meta name="description" content="Все скандальные новости и статьи про звезд">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <?php if (isset($error)) : ?>
        <?=$error ?>
    <?php else : ?>
    <div id="header">
        <h1>Интернет-журнал Желтый Вестник</h1>
        <h2>Все скандалы из мира шоу-бизнеса</h2>
    </div>
    <div id="content">
        <h3>Горячие новости на сегодня:</h3>
        <?=$newsViewer->write() ?>
        <h3>Лучшие статьи про звезд:</h3>
        <?=$articleViewer->write() ?>
    </div>
    <div id="footer">
        <h6>Интернет-журнал Желтый Вестник (с) 2019</h6>
    </div>
    <?php endif ?>
</div>
</body>
</html>
