<?php

require_once ('DatabaseConnect.php');
require_once ('Publication.php');
require_once ('News.php');
require_once ('Article.php');

if (isset($_GET['id'])){
    $id = (int)($_GET['id']);
} else {
    header('Location: index.php');
}

try {
    $publication = Publication::create($id);
} catch(Exception $error) {
    $error = 'Ошибка базы данных';
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Желтый Вестник: скандалы, интриги, расследования</title>
    <meta charset="utf-8">
    <meta name="description" content="Все скандальные новости и статьи про звезд">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <?php if (isset($error)) : ?>
        <?=$error ?>
    <?php else : ?>
    <div id="header">
        <h1>Интернет-журнал Желтый Вестник</h1>
        <h2>Все скандалы из мира шоу-бизнеса</h2>
    </div>
    <div id="content">
        <h4><?=$publication->getTitle() ?></h4>
        <p><?=$publication->getDescription() ?></p>
        <p><img src="images/<?=$id?>.jpg"></p>
        <p><?=$publication->getFullText() ?></p>
        <?php if ($publication instanceof News) : ?>
            <p>Источник: <?=$publication->getSource() ?></p>
        <?php else : ?>
            <p>Автор: <?=$publication->getAuthor() ?></p>
        <?php endif ?>
        <a class="btn btn-primary" href="index.php">Вернуться на главную</a>
    </div>
    <div id="footer">
        <h6>Интернет-журнал Желтый Вестник (с) 2019</h6>
    </div>
    <?php endif ?>
</div>
</body>
</html>

