<?php


class PublicationsViewer
{
    protected $publications;

    public function __construct($publicationType)
    {
        $db = new DatabaseConnect();

        $sql = 'select * from publications where type=:type';

        $query = $db->connection->prepare($sql);

        $query->bindValue(':type', $publicationType);

        $query->execute();

        $data = $query->fetchAll();

        if ($publicationType == 'news') {
            $news = [];
            foreach ($data as $publication) {
                $news[] = new News($publication['id'], $publication['title'], $publication['description'], $publication['fullText'], $publication['source']);
            }
            $this->publications = $news;
        } else {
            $articles = [];
            foreach ($data as $publication) {
                $articles[] = new Article($publication['id'], $publication['title'], $publication['description'], $publication['fullText'], $publication['author']);
            }
            $this->publications = $articles;
        }
    }

    public function write()
    {
        foreach ($this->publications as $publication){
            echo '<b>' . $publication->getTitle() . '</b><br>' . $publication->getDescription() . '<br><a href="info.php?id=' . $publication->getId() . '" class="btn btn-primary">Читать подробнее</a><br>';
        }
    }
}