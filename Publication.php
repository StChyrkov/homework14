<?php


class Publication
{
    protected $id;
    protected $title;
    protected $description;
    protected $fullText;


    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getFullText()
    {
        return $this->fullText;
    }


    public function __construct($id, $title, $description, $fullText)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->fullText = $fullText;
    }

    public static function create($id)
    {
        $db = new DatabaseConnect();

        $sql = 'select * from publications where id=:id';

        $query = $db->connection->prepare($sql);

        $query->bindValue(':id', $id);

        $query->execute();

        $data = $query->fetchObject();

        if ($data->type=='news'){
            return new News(
                $data->id,
                $data->title,
                $data->description,
                $data->fullText,
                $data->source
            );
        } else {
            return new Article(
                $data->id,
                $data->title,
                $data->description,
                $data->fullText,
                $data->author
            );
        }


    }

}