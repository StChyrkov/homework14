<?php


class News extends Publication
{
    protected $source;

    public function getSource()
    {
        return $this->source;
    }

    public function __construct($id, $title, $description, $fullText, $source)
    {
        parent::__construct($id, $title, $description, $fullText);
        $this->source = $source;
    }
}