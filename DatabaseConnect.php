<?php


class DatabaseConnect
{
    public $connection;

    public function __construct()
    {
            $this->connection = new PDO(
                'mysql:host=localhost;dbname=content',
                'content_user',
                '23021989'
            );

            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}