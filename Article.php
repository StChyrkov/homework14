<?php


class Article extends Publication
{
    protected $author;

    public function getAuthor()
    {
        return $this->author;
    }

    public function __construct($id, $title, $description, $fullText, $author)
    {
        parent::__construct($id, $title, $description, $fullText);
        $this->author = $author;
    }
}